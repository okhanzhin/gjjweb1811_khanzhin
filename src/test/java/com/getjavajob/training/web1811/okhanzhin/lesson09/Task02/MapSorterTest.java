package com.getjavajob.training.web1811.okhanzhin.lesson09.Task02;

import com.getjavajob.training.web1811.okhanzhin.lesson07.Task01.MapSorter;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class MapSorterTest {
    private MapSorter mapSorter;

    @Before
    public void init() {
        System.out.println("Initialize MapSorter Instance.");
        Map<Character, Integer> testMap = new HashMap<>();
        testMap.put('a', 10);
        testMap.put('b', 20);
        testMap.put('c', 50);
        this.mapSorter = new MapSorter(testMap);
    }

    @Test
    public void sortMapTest() {
        String excepted = "[c=50, b=20, a=10]";
        List<Map.Entry<Character, Integer>> sortedList = mapSorter.sortMap();
        System.out.println(sortedList.toString());
        assertEquals(excepted, sortedList.toString());
    }
}