package com.getjavajob.training.web1811.okhanzhin.lesson09.Task01;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;

import static org.junit.Assert.assertEquals;

public class InputUtilTest {

    @Test
    public void readFromConsoleTest() throws FileNotFoundException {
        CatalogReader excepted = new CatalogReader("/Volumes/Library/dev/projects/getjavajob/web/src/main/resources/catalog",
                "txt", "/Volumes/Library/dev/projects/getjavajob/web/src/test/testResources/InputUtilDummyLog.txt");
        CatalogReader actual = InputUtil.readFromConsole(new FileReader(
                "/Volumes/Library/dev/projects/getjavajob/web/src/test/testResources/InputUtilConsoleTest.txt"));

        assertEquals(excepted, actual);
    }

    @Test
    public void readFromConsoleWithoutExtensionTest() throws FileNotFoundException {
        CatalogReader excepted = new CatalogReader("/Volumes/Library/dev/projects/getjavajob/web/src/main/resources/catalog",
                "/Volumes/Library/dev/projects/getjavajob/web/src/test/testResources/InputUtilDummyLog.txt");
        CatalogReader actual = InputUtil.readFromConsole(new FileReader(
                "/Volumes/Library/dev/projects/getjavajob/web/src/test/testResources/InputUtilConsoleWithoutExtensionTest.txt"));

        assertEquals(excepted, actual);
    }
}