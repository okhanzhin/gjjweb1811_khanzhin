package com.getjavajob.training.web1811.okhanzhin.lesson08.Task01;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class MultiThreadMultiplierTest {

    @Test
    public void runTest() throws InterruptedException {
        int[][] inputMatrix1 = new int[][]{{1, 1, 1}, {2, 2, 2}, {3, 3, 3}};
        int[][] inputMatrix2 = new int[][]{{1, 1, 1}, {2, 2, 2}, {3, 3, 3}};
        int[][] excepted = new int[][]{{6, 6, 6}, {12, 12, 12}, {18, 18, 18}};
        int[][] actual = new int[inputMatrix1.length][inputMatrix2[0].length];
        Thread[][] threads = new Thread[inputMatrix1.length][inputMatrix2[0].length];

        for (int row = 0; row < inputMatrix1.length; row++) {
            for (int col = 0; col < inputMatrix2[0].length; col++) {
                Thread multiplyThread = new Thread(new MultiThreadMultiplier(row, col, inputMatrix1, inputMatrix2, actual));
                threads[row][col] = multiplyThread;
                multiplyThread.start();
                threads[row][col].join();
            }
        }

        assertTrue(Arrays.deepEquals(excepted, actual));
    }
}