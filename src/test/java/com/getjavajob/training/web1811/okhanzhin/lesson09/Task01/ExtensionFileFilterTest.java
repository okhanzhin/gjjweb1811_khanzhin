package com.getjavajob.training.web1811.okhanzhin.lesson09.Task01;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertTrue;

public class ExtensionFileFilterTest {

    @Test
    public void acceptTest() {
        ExtensionFileFilter filter = new ExtensionFileFilter("txt");

        assertTrue(filter.accept(new File(
                "/Volumes/Library/dev/projects/getjavajob/web/src/main/resources/alphabet1251.txt"), "txt"));
    }
}