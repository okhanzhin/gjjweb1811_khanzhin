package com.getjavajob.training.web1811.okhanzhin.lesson09.Task02;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.assertEquals;

public class CountDownCharSearcherTest {

    @Test
    public void callTest() throws ExecutionException, InterruptedException {
        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();

        int actual = cachedThreadPool.submit(new CountDownCharSearcher('a', "abrakadabra",
                                             new CountDownLatch(1))).get();

        assertEquals(5, actual);
    }
}