package com.getjavajob.training.web1811.okhanzhin.lesson09.Task02;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class StringUtilTest {

    @Test
    public void prepareStringText() throws IOException {
        StringUtil stringUtil = new StringUtil();
        String excepted = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        assertEquals(excepted, stringUtil.readFile(
                "/Volumes/Library/dev/projects/getjavajob/web/src/main/resources/alphabet1251.txt"));
    }

    @Test
    public void fileNotFoundExceptionTest(){
        StringUtil stringUtil = new StringUtil();
        assertThrows(FileNotFoundException.class, () -> stringUtil.readFile(
                "/Volumes/Library/dev/projects/getjavajob/web/src/main/resources/abc.txt"));
    }
}