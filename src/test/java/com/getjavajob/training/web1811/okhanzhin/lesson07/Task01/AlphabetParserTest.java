package com.getjavajob.training.web1811.okhanzhin.lesson07.Task01;
import org.junit.Before;
import org.junit.Test;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class AlphabetParserTest {
    private AlphabetParser alphabetParser;

    @Before
    public void init() {
        System.out.println("Initialize AlphabetParser Instance.");
        this.alphabetParser = new AlphabetParser("абвгдеёжзийклмнопрстуфхцчшщъыьэюя", "абвгдеёжзийклмнопрстуфхцчшщъыьэюя");
    }

    @Test
    public void testParseByAlphabet() throws InterruptedException {
        String excepted = "[р=1, с=1, т=1, у=1, ф=1, х=1, ц=1, ч=1, ш=1, щ=1, ъ=1, " +
                           "ы=1, ь=1, э=1, ю=1, я=1, ё=1, а=1, б=1, в=1, г=1, д=1, " +
                           "е=1, ж=1, з=1, и=1, й=1, к=1, л=1, м=1, н=1, о=1, п=1]";

        this.alphabetParser.parseByAlphabet();
        assertEquals(excepted, Arrays.toString(alphabetParser.getCharMap().entrySet().toArray()));
    }
}
