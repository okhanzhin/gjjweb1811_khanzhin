package com.getjavajob.training.web1811.okhanzhin.lesson08.Task01;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class MultiThreadExecutorTest {

    @Test
    public void multiplyTest() throws InterruptedException {
        int[][] inputMatrix1 = new int[][]{{1, 1, 1}, {2, 2, 2}, {3, 3, 3}};
        int[][] inputMatrix2 = new int[][]{{1, 1, 1}, {2, 2, 2}, {3, 3, 3}};
        int[][] excepted = new int[][]{{6, 6, 6}, {12, 12, 12}, {18, 18, 18}};

        MultiThreadExecutor multiThreadExecutor = new MultiThreadExecutor(inputMatrix1, inputMatrix2);
        multiThreadExecutor.multiply();

        assertTrue(Arrays.deepEquals(excepted, multiThreadExecutor.getResult()));
    }
}