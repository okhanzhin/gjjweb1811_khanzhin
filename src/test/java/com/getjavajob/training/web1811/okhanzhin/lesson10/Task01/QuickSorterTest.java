package com.getjavajob.training.web1811.okhanzhin.lesson10.Task01;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;

import static org.junit.Assert.assertTrue;

public class QuickSorterTest {
    private final static int TEN_MILLIONS = 10_000_000;
    private Integer[] testArray;

    @Before
    public void init() {
        this.testArray = new Integer[TEN_MILLIONS];

        int range = 10 * testArray.length;
        Random random = new Random();

        for (int i = 0; i < testArray.length; i++) {
            testArray[i] = random.nextInt(2 * range);
        }
    }

    @Test
    public void computeTest() {
        Integer[] excepted = this.testArray;
        Integer[] actual = this.testArray;

        Arrays.sort(excepted);

        ForkJoinPool quickSortPool = new ForkJoinPool();
        QuickSorter<Integer> sorter = new QuickSorter<>(actual, 0, actual.length - 1);

        quickSortPool.execute(sorter);

        assertTrue(Arrays.deepEquals(excepted, actual));
    }
}