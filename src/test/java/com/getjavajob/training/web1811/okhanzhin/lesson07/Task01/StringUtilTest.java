package com.getjavajob.training.web1811.okhanzhin.lesson07.Task01;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class StringUtilTest {
    private StringUtil stringUtil;

    @Test
    public void prepareStringText() throws IOException {
        this.stringUtil = new StringUtil();
        String excepted = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        assertEquals(excepted, this.stringUtil.prepareString(
                    "/Volumes/Library/dev/projects/getjavajob/web/src/main/resources/alphabet1251.txt"));
    }

    @Test
    public void fileNotFoundExceptionTest(){
        this.stringUtil = new StringUtil();
        assertThrows(FileNotFoundException.class, () -> this.stringUtil.prepareString(
                    "/Volumes/Library/dev/projects/getjavajob/web/src/main/resources/abc.txt"));
    }
}
