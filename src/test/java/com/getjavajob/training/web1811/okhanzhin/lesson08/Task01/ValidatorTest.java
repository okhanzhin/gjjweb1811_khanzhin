package com.getjavajob.training.web1811.okhanzhin.lesson08.Task01;

import org.junit.Test;

import static com.getjavajob.training.web1811.okhanzhin.lesson08.Task01.Validator.validate;
import static org.junit.Assert.assertThrows;

public class ValidatorTest {

    @Test
    public void IllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> validate(new int[][]{{1, 1, 1}, {2, 2, 2}},
                                                                    new int[][]{{1, 1, 1}, {2, 2, 2}}));
    }
}