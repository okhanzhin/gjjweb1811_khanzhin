package com.getjavajob.training.web1811.okhanzhin.lesson09.Task03var02;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Exchanger;

import static org.junit.Assert.assertArrayEquals;

public class ProcessorTest {

    @Test
    public void process() {
        Exchanger<Integer> evenExchanger = new Exchanger<>();
        Exchanger<Integer> oddExchanger = new Exchanger<>();
        List<Integer> evenNumbers = new ArrayList<>();
        List<Integer> oddNumbers = new ArrayList<>();
        List<Integer> allGeneratedNumbers = new ArrayList<>();

        Thread producer = new Thread(new Producer(evenExchanger, oddExchanger, 10, allGeneratedNumbers));
        producer.start();

        Thread evenConsumer = new Thread(new ConsumerEven(evenExchanger, evenNumbers));
        evenConsumer.start();
        Thread oddConsumer = new Thread(new ConsumerOdd(oddExchanger, oddNumbers));
        oddConsumer.start();

        try {
            producer.join();
            evenConsumer.join();
            oddConsumer.join();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        List<Integer> evenOddConcat = new ArrayList<>();
        evenOddConcat.addAll(evenNumbers);
        evenOddConcat.addAll(oddNumbers);

        Collections.sort(allGeneratedNumbers);
        Collections.sort(evenOddConcat);

        assertArrayEquals(allGeneratedNumbers.toArray(), evenOddConcat.toArray());
    }
}