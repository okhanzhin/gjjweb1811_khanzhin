package com.getjavajob.training.web1811.okhanzhin.lesson07.Task02;

import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class MultiThreadQuickSortTest {

    @Test
    public void multiThreadQuickSort() {
        Integer[] actual = new Integer[]{1,5,7,5,43,66,77,3,51,63};
        Integer[] excepted = new Integer[]{1,5,7,5,43,66,77,3,51,63};
        MultiThreadQuickSort<Integer> oneThreadQuickSort = new MultiThreadQuickSort<>();

        Arrays.sort(excepted);
        oneThreadQuickSort.multiThreadQuickSort(actual, 0, actual.length - 1);
        System.out.println(Arrays.toString(actual));

        assertEquals(excepted, actual);
    }

    @Test
    public void multiThreadQuickSortEmptyArray() {
        MultiThreadQuickSort<Integer> oneThreadQuickSort = new MultiThreadQuickSort<>();
        Integer[] array = {};
        assertThrows(IllegalArgumentException.class, () -> oneThreadQuickSort.multiThreadQuickSort(array, 0, array.length - 1));
    }
}