package com.getjavajob.training.web1811.okhanzhin.lesson09.Task01;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

public class CatalogReaderTest {

    @Test
    public void readCatalogElementsWithFileExtensionTest() throws IOException {
        CatalogReader reader = new CatalogReader(
                "/Volumes/Library/dev/projects/getjavajob/web/src/main/resources/catalog", "txt",
                "/Volumes/Library/dev/projects/getjavajob/web/src/test/testResources/OutputTestLog.txt");
        reader.readCatalogElements();

        assertEquals(new String(Files.readAllBytes(Paths.get(
                "/Volumes/Library/dev/projects/getjavajob/web/src/test/testResources/ExceptedLog.txt"))),
                     new String(Files.readAllBytes(Paths.get(
                "/Volumes/Library/dev/projects/getjavajob/web/src/test/testResources/OutputTestLog.txt"))));
    }

    @Test
    public void readCatalogElementsWithoutFileExtensionTest() throws IOException {
        CatalogReader reader = new CatalogReader(
                "/Volumes/Library/dev/projects/getjavajob/web/src/main/resources/catalog",
                "/Volumes/Library/dev/projects/getjavajob/web/src/test/testResources/OutputWithExtensionTestLog.txt");
        reader.readCatalogElements();

        assertEquals(new String(Files.readAllBytes(Paths.get(
                "/Volumes/Library/dev/projects/getjavajob/web/src/test/testResources/ExceptedLogWithoutExtension.txt"))),
                     new String(Files.readAllBytes(Paths.get(
                "/Volumes/Library/dev/projects/getjavajob/web/src/test/testResources/OutputWithExtensionTestLog.txt"))));
    }
}