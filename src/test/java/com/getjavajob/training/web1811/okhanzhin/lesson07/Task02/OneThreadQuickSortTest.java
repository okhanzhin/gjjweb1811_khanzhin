package com.getjavajob.training.web1811.okhanzhin.lesson07.Task02;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import java.util.Arrays;

public class OneThreadQuickSortTest {

    @Test
    public void quicksortRegularTest() throws IllegalArgumentException {
        Integer[] actual = new Integer[]{1,5,7,5,43,66,77,3,51,63};
        Integer[] excepted = new Integer[]{1,5,7,5,43,66,77,3,51,63};
        OneThreadQuickSort<Integer> oneThreadQuickSort = new OneThreadQuickSort<>();

        Arrays.sort(excepted);
        oneThreadQuickSort.quicksort(actual, 0, actual.length - 1);

        assertEquals(excepted, actual);
    }

    @Test
    public void quicksortEmptyArray() {
        OneThreadQuickSort<Integer> oneThreadQuickSort = new OneThreadQuickSort<>();
        Integer[] array = {};
        assertThrows(IllegalArgumentException.class, () -> oneThreadQuickSort.quicksort(array, 0, array.length - 1));
    }
}