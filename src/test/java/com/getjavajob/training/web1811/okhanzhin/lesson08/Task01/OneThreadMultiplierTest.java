package com.getjavajob.training.web1811.okhanzhin.lesson08.Task01;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class OneThreadMultiplierTest {

    @Test
    public void multiplyTest() {
        int[][] inputMatrix1 = new int[][]{{1, 1, 1}, {2, 2, 2}, {3, 3, 3}};
        int[][] inputMatrix2 = new int[][]{{1, 1, 1}, {2, 2, 2}, {3, 3, 3}};
        int[][] excepted = new int[][]{{6, 6, 6}, {12, 12, 12}, {18, 18, 18}};

        OneThreadMultiplier multiplier = new OneThreadMultiplier();

        assertTrue(Arrays.deepEquals(excepted, multiplier.multiply(inputMatrix1, inputMatrix2)));
    }
}