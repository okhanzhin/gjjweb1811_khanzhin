package com.getjavajob.training.web1811.okhanzhin.lesson08.Task01;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

public class WriterTest {

    @Test
    public void writeToFileTest() throws IOException {
        Writer writer = new Writer();
        writer.writeToFile(new int[][]{{6, 6, 6}, {12, 12, 12}, {18, 18, 18}},
                "/Volumes/Library/dev/projects/getjavajob/web/src/test/testResources/TestOutputMatrix.txt");

        assertEquals(new String(Files.readAllBytes(Paths.get(
                "/Volumes/Library/dev/projects/getjavajob/web/src/test/testResources/ExceptedOutputMatrix.txt"))),
                     new String(Files.readAllBytes(Paths.get(
                "/Volumes/Library/dev/projects/getjavajob/web/src/test/testResources/TestOutputMatrix.txt"))));
    }
}