package com.getjavajob.training.web1811.okhanzhin.lesson09.Task03var01;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertArrayEquals;

public class ProcessorTest {

    @Test
    public void processTest() {
        AtomicInteger buffer = new AtomicInteger();
        List<Integer> evenNumbers = new ArrayList<>();
        List<Integer> oddNumbers = new ArrayList<>();
        List<Integer> allGeneratedNumbers = new ArrayList<>();

        Thread producer = new Thread(new Producer(buffer, 10, allGeneratedNumbers));
        producer.start();

        Thread evenConsumer = new Thread(new ConsumerEven(buffer, evenNumbers));
        Thread oddConsumer = new Thread(new ConsumerOdd(buffer, oddNumbers));
        evenConsumer.start();
        oddConsumer.start();

        try {
            producer.join();
            evenConsumer.join();
            oddConsumer.join();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        List<Integer> evenOddConcat = new ArrayList<>();
        evenOddConcat.addAll(evenNumbers);
        evenOddConcat.addAll(oddNumbers);

        Collections.sort(allGeneratedNumbers);
        Collections.sort(evenOddConcat);

        assertArrayEquals(allGeneratedNumbers.toArray(), evenOddConcat.toArray());
    }
}