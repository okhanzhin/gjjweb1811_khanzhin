package com.getjavajob.training.web1811.okhanzhin.lesson08.Task01;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.Arrays;

import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

public class ReaderTest {

    @Test
    public void readFromFileTest() throws FileNotFoundException {
        Reader reader = new Reader("/Volumes/Library/dev/projects/getjavajob/web/src/main/resources/matrix1.txt");
        int[][] excepted = new int[][]{{1, 1, 1}, {2, 2, 2}, {3, 3, 3}};

        assertTrue(Arrays.deepEquals(excepted, reader.read()));
    }

    @Test
    public void fileNotFoundExceptionTest() {
        Reader reader = new Reader(" ");
        assertThrows(FileNotFoundException.class, reader::read);
    }
}