package com.getjavajob.training.web1811.okhanzhin.lesson10.Task01;

import java.util.concurrent.RecursiveAction;

public class QuickSorter<E extends Comparable<E>> extends RecursiveAction {

    private static final long serialVersionUID = 1L;
    private static final int threshold = 1000;

    private E[] array;
    private int startIndex;
    private int finishIndex;

    public QuickSorter(E[] array, int startIndex, int finishIndex) {
        this.array = array;
        this.startIndex = startIndex;
        this.finishIndex = finishIndex;
    }

    @Override
    protected void compute() {
        if (array.length == 0) {
            throw new IllegalArgumentException("Array is empty.");
        } else if (finishIndex - startIndex <= threshold) {
            insertionSort(array, startIndex, finishIndex);
        } else {
            int i = this.partition(array, startIndex, finishIndex);

            QuickSorter<E> leftSorter = new QuickSorter<>(array, startIndex, i - 1);
            QuickSorter<E> rightSorter = new QuickSorter<>(array, i + 1, finishIndex);
            leftSorter.invoke();
            rightSorter.invoke();
        }
    }

    private void insertionSort(E[] array, int low, int high) {
        for (int outerCount = low; outerCount < high; outerCount++) {
            E temp = array[outerCount];
            int innerCount = outerCount;
            while (innerCount > 0 && array[innerCount - 1].compareTo(temp) >= 0) {
                array[innerCount] = array[innerCount - 1];
                --innerCount;
            }

            array[innerCount] = temp;
        }
    }

    private int partition(E[] array, int low, int high) {
        E pivot = array[high];

        for (int i = low; i < high; i++) {
            if (array[i].compareTo(pivot) < 0) {
                E temp = array[low];
                array[low] = array[i];
                array[i] = temp;
                low++;
            }
        }

        E temp = array[low];
        array[low] = pivot;
        array[high] = temp;

        return low;
    }
}
