package com.getjavajob.training.web1811.okhanzhin.lesson08.Task01;

public class Validator {
    public static void validate(int[][] a, int[][] b) throws IllegalArgumentException {
        if (a[0].length != b.length) {
            throw new IllegalArgumentException("Entered matrices can't be multiply!");
        }
    }
}
