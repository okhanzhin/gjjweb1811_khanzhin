package com.getjavajob.training.web1811.okhanzhin.lesson09.Task01;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Reader;
import java.util.Scanner;

public class InputUtil {

    public static CatalogReader readFromConsole(Reader reader) throws FileNotFoundException {
        Scanner scanner = new Scanner(reader);

        System.out.println("Enter folder path: ");
        String path = scanner.nextLine();

        while (!validate(path)) {
            System.out.println("Wrong folder path, please try again.");
            path = scanner.nextLine();
        }

        System.out.println("Enter file extension, or press 'Enter' to skip: ");;

        if (scanner.hasNextLine()) {
            return new CatalogReader(path, scanner.nextLine(),
                    "/Volumes/Library/dev/projects/getjavajob/web/src/test/testResources/InputUtilDummyLog.txt");
        } else {
            return new CatalogReader(path,
                    "/Volumes/Library/dev/projects/getjavajob/web/src/test/testResources/InputUtilDummyLog.txt");
        }
    }

    private static boolean validate(String path) {
        File root = new File(path);
        return root.isDirectory();
    }

}
