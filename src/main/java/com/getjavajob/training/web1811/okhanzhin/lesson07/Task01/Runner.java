package com.getjavajob.training.web1811.okhanzhin.lesson07.Task01;

import java.util.Arrays;

public class Runner {
    public static void main(String[] args) throws Exception {
        StringUtil stringUtil = new StringUtil();
        AlphabetParser alphabetParser = new AlphabetParser(stringUtil.prepareString(
                               "/Volumes/Library/dev/projects/getjavajob/web/src/main/resources/text1251.txt"),
                                        stringUtil.prepareString(
                               "/Volumes/Library/dev/projects/getjavajob/web/src/main/resources/alphabet1251.txt"));
        alphabetParser.parseByAlphabet();
        MapSorter mapSorter = new MapSorter(alphabetParser.getCharMap());

        System.out.println(Arrays.toString(mapSorter.sortMap().toArray()));
    }
}

