package com.getjavajob.training.web1811.okhanzhin.lesson09.Task01;

import java.io.FileNotFoundException;
import java.io.InputStreamReader;

public class Runner {
    public static void main(String[] args) {
        try {
            CatalogReader cr = InputUtil.readFromConsole(new InputStreamReader(System.in));
            cr.readCatalogElements();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
