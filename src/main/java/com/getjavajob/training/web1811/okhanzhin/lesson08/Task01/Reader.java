package com.getjavajob.training.web1811.okhanzhin.lesson08.Task01;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Reader {
    private static final int ZERO = 0;
    private static final String SPACE = " ";
    public final String source;

    public Reader(String source) {
        this.source = source;
    }

    public int[][] read() throws FileNotFoundException {
        Scanner matrixScanner = new Scanner(new File(source));
        int rowNums = ZERO;

        while (matrixScanner.hasNextLine()) {
            ++rowNums;
            matrixScanner.nextLine();
        }
        matrixScanner.close();

        return fillMatrix(source, rowNums);
    }

    private int[][] fillMatrix(String source, int rowNums) throws FileNotFoundException {
        int[][] matrix = new int[rowNums][];
        Scanner matrixScanner = new Scanner(new File(source));

        while (matrixScanner.hasNextLine()) {
            for (int i = 0; i < rowNums; i++) {
                String line = matrixScanner.nextLine();
                String[] token = line.split(SPACE);
                matrix[i] = new int[token.length];

                for (int j = 0; j < token.length; j++) {
                    matrix[i][j] = Integer.parseInt(token[j]);
                }
            }
        }
        matrixScanner.close();

        return matrix;
    }
}
