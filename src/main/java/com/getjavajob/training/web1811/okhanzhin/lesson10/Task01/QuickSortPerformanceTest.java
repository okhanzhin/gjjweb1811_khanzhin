package com.getjavajob.training.web1811.okhanzhin.lesson10.Task01;

import com.getjavajob.training.web1811.okhanzhin.lesson07.Task02.MultiThreadQuickSort;
import com.getjavajob.training.web1811.okhanzhin.lesson07.Task02.OneThreadQuickSort;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;

public class QuickSortPerformanceTest {
    private final static int TEN_MILLIONS = 10_000_000;

    public static void main(String[] args) {

        OneThreadQuickSort<Integer> single = new OneThreadQuickSort<>();
        Integer[] testArray1 = generateArray(TEN_MILLIONS);
        long startTime = System.currentTimeMillis();

        single.quicksort(testArray1, 0, testArray1.length - 1);
        System.out.println("Single array sorted in " + (System.currentTimeMillis() - startTime) + " ms");

        MultiThreadQuickSort<Integer> multi = new MultiThreadQuickSort<>();
        Integer[] testArray2 = generateArray(TEN_MILLIONS);

        startTime = System.currentTimeMillis();

        multi.multiThreadQuickSort(testArray2, 0, testArray2.length - 1);
        System.out.println("Threaded array sorted in " + (System.currentTimeMillis() - startTime) + " ms");

        ForkJoinPool quickSortPool = new ForkJoinPool();
        Integer[] testArray3 = generateArray(TEN_MILLIONS);
        QuickSorter<Integer> sorter = new QuickSorter<>(testArray3, 0, testArray3.length - 1);

        startTime = System.currentTimeMillis();

        quickSortPool.execute(sorter);
        System.out.println("Fork/Join array sorted in " + (System.currentTimeMillis() - startTime) + " ms");
    }

    public static Integer[] generateArray(int capacity) {
        Integer[] array = new Integer[capacity];

        int range = 10 * array.length;
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(2 * range);
        }

        return array;
    }
}
