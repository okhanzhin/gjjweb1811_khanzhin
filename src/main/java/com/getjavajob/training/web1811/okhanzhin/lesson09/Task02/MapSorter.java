package com.getjavajob.training.web1811.okhanzhin.lesson09.Task02;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class MapSorter {
    private Map<Character, Integer> sortMap;

    public MapSorter(Map<Character, Integer> sortMap) {
        this.sortMap = sortMap;
    }

    public List<Map.Entry<Character, Integer>> sortMap() {
        Comparator<Map.Entry<Character, Integer>> comparator = (o1, o2) -> o2.getValue().compareTo(o1.getValue());
        List<Map.Entry<Character, Integer>> listOfCharacters = new ArrayList<>(sortMap.entrySet());
        listOfCharacters.sort(comparator);

        return listOfCharacters;
    }
}
