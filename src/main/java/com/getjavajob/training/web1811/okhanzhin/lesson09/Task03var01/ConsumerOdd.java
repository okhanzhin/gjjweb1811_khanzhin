package com.getjavajob.training.web1811.okhanzhin.lesson09.Task03var01;

import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class ConsumerOdd implements Runnable {
    private static Semaphore oddSem = new Semaphore(0);
    private AtomicInteger buffer;
    private List<Integer> outputArray;

    public ConsumerOdd(AtomicInteger buffer, List<Integer> outputArray) {
        this.buffer = buffer;
        this.outputArray = outputArray;
    }

    public static void releaseSemaphore() {
        oddSem.release();
    }

    @Override
    public void run() {
        while (true) {
            try {
                oddSem.acquire();

                if (Producer.isGenerationFinished()) {
                    break;
                }

                Producer.acquireSemaphore();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            outputArray.add(buffer.get());
            System.out.println("Odd " + outputArray.get(outputArray.size() - 1));

            Producer.releaseSemaphore();
            System.out.println("Producer Sem released");
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("ConsumerOdd is finished");
    }
}
