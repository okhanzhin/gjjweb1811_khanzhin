package com.getjavajob.training.web1811.okhanzhin.lesson08.Task01;

import java.io.FileNotFoundException;

public class OneThreadRunner {
    public static void main(String[] args) {
        Reader reader1 = new Reader(
                "/Volumes/Library/dev/projects/getjavajob/web/src/main/resources/matrix1.txt");
        Reader reader2 = new Reader(
                "/Volumes/Library/dev/projects/getjavajob/web/src/main/resources/matrix2.txt");
        
        try {
            int[][] matrix1 = reader1.read();
            int[][] matrix2 = reader2.read();

            Validator.validate(matrix1, matrix2);

            OneThreadMultiplier oneThreadMultiplier = new OneThreadMultiplier();
            Writer writer = new Writer();

            writer.writeToFile(oneThreadMultiplier.multiply(matrix1, matrix2),
                    "/Volumes/Library/dev/projects/getjavajob/web/src/main/resources/OutputMatrix.txt");
        } catch (FileNotFoundException | IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
