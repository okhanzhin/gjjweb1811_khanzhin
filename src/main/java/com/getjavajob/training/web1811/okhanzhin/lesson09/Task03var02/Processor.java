package com.getjavajob.training.web1811.okhanzhin.lesson09.Task03var02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Exchanger;

public class Processor {
    public static void main(String[] args) {
        Processor pr = new Processor();
        pr.process();
    }

    public void process() {
        Exchanger<Integer> evenExchanger = new Exchanger<>();
        Exchanger<Integer> oddExchanger = new Exchanger<>();

        Thread producer = new Thread(new Producer(evenExchanger, oddExchanger, 10, new ArrayList<>()));
        producer.start();

        List<Integer> evenNumbers = new ArrayList<>();
        List<Integer> oddNumbers = new ArrayList<>();

        Thread evenConsumer = new Thread(new ConsumerEven(evenExchanger, evenNumbers));
        Thread oddConsumer = new Thread(new ConsumerOdd(oddExchanger, oddNumbers));

        evenConsumer.start();
        oddConsumer.start();

        try {
            producer.join();
            evenConsumer.join();
            oddConsumer.join();

            System.out.println(Arrays.toString(evenNumbers.toArray()));
            System.out.println(Arrays.toString(oddNumbers.toArray()));
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
