package com.getjavajob.training.web1811.okhanzhin.lesson09.Task03var02;

import java.util.List;
import java.util.concurrent.Exchanger;

public class ConsumerOdd implements Runnable {
    private Exchanger<Integer> exchanger;
    private List<Integer> outputArray;

    public ConsumerOdd(Exchanger<Integer> exchanger, List<Integer> outputArray) {
        this.exchanger = exchanger;
        this.outputArray = outputArray;
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (!Producer.isGenerationFinished()) {
                    outputArray.add(exchanger.exchange(null));
                    System.out.println("Odd Consumer exchange number");
                } else {
                    outputArray.remove(outputArray.size() - 1);
                    break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("Odd " + outputArray.get(outputArray.size() - 1));
        }

        System.out.println("ConsumerOdd is finished");
    }
}
