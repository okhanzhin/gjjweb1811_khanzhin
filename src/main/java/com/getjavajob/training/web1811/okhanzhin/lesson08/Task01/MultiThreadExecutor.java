package com.getjavajob.training.web1811.okhanzhin.lesson08.Task01;

public class MultiThreadExecutor {
    private int[][] matrix1;
    private int[][] matrix2;
    private int[][] result;

    public MultiThreadExecutor(int[][] matrix1, int[][] matrix2) {
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;
    }

    public int[][] getResult() {
        return result;
    }

    public void multiply() throws InterruptedException {
        int rowDimension = matrix1.length;
        int colDimension = matrix2[0].length;
        result = new int[rowDimension][colDimension];
        Thread[][] threads = new Thread[rowDimension][colDimension];

        for (int row = 0; row < rowDimension; row++) {
            for (int col = 0; col < colDimension; col++) {
                Thread multiplyThread = new Thread(new MultiThreadMultiplier(row, col, matrix1, matrix2, result));
                threads[row][col] = multiplyThread;
                multiplyThread.start();
            }
        }

        for (int row = 0; row < rowDimension; row++) {
            for (int col = 0; col < colDimension; col++) {
                System.out.println("Waiting for thread to finish " + threads[row][col].getName());
                threads[row][col].join();
                System.out.println(threads[row][col].getName() + " finished.");
            }
        }
    }
}
