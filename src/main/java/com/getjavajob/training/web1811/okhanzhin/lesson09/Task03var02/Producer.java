package com.getjavajob.training.web1811.okhanzhin.lesson09.Task03var02;

import java.util.List;
import java.util.Random;
import java.util.concurrent.Exchanger;

public class Producer implements Runnable {
    private Exchanger<Integer> evenExchanger;
    private Exchanger<Integer> oddExchanger;
    private List<Integer> outputArray;
    private int iterationNums;

    private static boolean generationFinished = false;

    public Producer(Exchanger<Integer> evenExchanger, Exchanger<Integer> oddExchanger, int iterationNums, List<Integer> outputArray) {
        this.iterationNums = iterationNums;
        this.evenExchanger = evenExchanger;
        this.oddExchanger = oddExchanger;
        this.outputArray = outputArray;
    }

    public static void setGenerationFinished(boolean generationFinished) {
        Producer.generationFinished = generationFinished;
    }

    public static boolean isGenerationFinished() {
        return generationFinished;
    }

    @Override
    public void run() {
        Random random = new Random();

        for (int i = 0; i < iterationNums; i++) {
            int buffer = random.nextInt(100);
            outputArray.add(buffer);
            try {
                if (buffer % 2 == 0) {
                    evenExchanger.exchange(buffer);
                } else {
                    oddExchanger.exchange(buffer);
                }

                Thread.sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        setGenerationFinished(true);

        try {
            evenExchanger.exchange(null);
            oddExchanger.exchange(null);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        System.out.println("Producer is finished");
    }
}