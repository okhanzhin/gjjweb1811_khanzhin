package com.getjavajob.training.web1811.okhanzhin.lesson09.Task02;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

public class CountDownCharSearcher implements Callable<Integer> {
    private char letter;
    private String localSource;
    private CountDownLatch countDownLatch;

    public CountDownCharSearcher(char letter, String localSource, CountDownLatch countDownLatch) {
        this.letter = letter;
        this.localSource = localSource;
        this.countDownLatch = countDownLatch;
    }

    @Override
    public Integer call() {
        System.out.println("Thread is started " + this.getClass());
        AtomicInteger occurrence = new AtomicInteger(0);
        for (char c : localSource.toCharArray()) {
            if (c == letter) {
                occurrence.getAndIncrement();
            }
        }
        countDownLatch.countDown();

        return occurrence.get();
    }
}
