package com.getjavajob.training.web1811.okhanzhin.lesson08.Task01;

public class MultiThreadMultiplier implements Runnable {
    private int row;
    private int col;
    private int[][] matrix1;
    private int[][] matrix2;
    private int[][] output;

    public MultiThreadMultiplier(int row, int col, int[][] matrix1, int[][] matrix2, int[][] output) {
        this.row = row;
        this.col = col;
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;
        this.output = output;
    }

    @Override
    public void run() {
        output[row][col] = 0;
        for (int i = 0; i < output[row].length; i++) {
            output[row][col] += matrix1[row][i] * matrix2[i][col];
        }
    }
}
