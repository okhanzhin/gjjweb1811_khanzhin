package com.getjavajob.training.web1811.okhanzhin.lesson09.Task01;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class CatalogReader {
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

    String dirPath;
    String fileExt;
    private File outputLogFile;

    public CatalogReader(String dirPath, String logFileName) {
        this.dirPath = dirPath;
        this.outputLogFile = new File(logFileName);
    }

    public CatalogReader(String dirPath, String fileExt, String logFileName) {
        this.dirPath = dirPath;
        this.fileExt = fileExt;
        this.outputLogFile = new File(logFileName);
    }

    public void readCatalogElements() {
        File root = new File(dirPath);

        try {
            FileWriter logWriter = new FileWriter(this.outputLogFile);

            for (File item : root.listFiles()) {
                if (item.isDirectory()) {
                    logWriter.write(printItem(item));
                    logWriter.write("\n");
                    System.out.println(printItem(item));

                    logWriter.write(readSubFolder(item.getAbsolutePath(), fileExt));
                } else {
                    logWriter.write(printItem(item));
                    logWriter.write("\n");
                    System.out.println(printItem(item));
                }
            }

            logWriter.close();
        } catch (NullPointerException | IOException e) {
            System.out.println("Root is Empty.");
        }
    }

    private String readSubFolder(String subFolderPath, String fileExt) {
        File subFolder = new File(subFolderPath);
        StringBuilder subLogBuilder = new StringBuilder();

        if (fileExt != null) {
            try {
                File[] subFolderList = subFolder.listFiles(new ExtensionFileFilter(fileExt));

                if (subFolderList != null && subFolderList.length != 0) {
                    for (File subItem : subFolder.listFiles()) {
                        if (subItem.isDirectory()) {
                            subLogBuilder.append(printItem(subItem));
                            subLogBuilder.append("\n");
                            System.out.println(printItem(subItem));

                            readSubFolder(subItem.getAbsolutePath(), fileExt);
                        } else if (subItem.isFile()) {
                            System.out.println(printItem(subItem));

                            subLogBuilder.append(printItem(subItem));
                            subLogBuilder.append("\n");
                        }
                    }
                }
            } catch (NullPointerException e) {
                System.out.println(subFolder.getName() + " is Empty.");
            }

        }

        return subLogBuilder.toString();
    }

    private String printItem(File item) {
        StringBuilder sb = new StringBuilder();

        if (item.isDirectory()) {
            sb.append("d")
                    .append(checkPermissions(item))
                    .append(simpleDateFormat.format(item.lastModified()))
                    .append(" ")
                    .append(item.getName());
        } else if (item.isFile()) {
            sb.append(checkPermissions(item))
                    .append(convertSize(item.length()))
                    .append(" ")
                    .append(simpleDateFormat.format(item.lastModified()))
                    .append(" ")
                    .append(item.getName());
        }

        return sb.toString();
    }

    private String checkPermissions(File file) {
        if (file.canRead() && file.canWrite() && file.canExecute()) {
            return " rwx ";
        }

        return " ";
    }

    private String convertSize(long size) {
        if (size == 0) {
            return "0";
        }

        final String[] units = new String[]{"B", "kB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        CatalogReader cr = (CatalogReader) obj;

        if (this.fileExt != null && cr.fileExt != null) {
            return (dirPath.equals(cr.dirPath)) &&
                    ((fileExt.equals(cr.fileExt))) &&
                    (outputLogFile.getName().equals(cr.outputLogFile.getName()));
        } else if (this.fileExt == null && cr.fileExt == null) {
            return (dirPath.equals(cr.dirPath)) &&
                    (outputLogFile.getName().equals(cr.outputLogFile.getName()));
        } else {
            return false;
        }
    }
}
