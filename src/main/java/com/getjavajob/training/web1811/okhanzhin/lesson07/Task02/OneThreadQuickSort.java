package com.getjavajob.training.web1811.okhanzhin.lesson07.Task02;

public class OneThreadQuickSort<E extends Comparable<E>> {

    public void quicksort(E[] array, int low, int high) throws IllegalArgumentException {
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException("Error at OneThreadQuickSort::quicksort. Input array of data to be sorted is empty or null.");
        } else if (high > low) {
            int i = this.partition(array, low, high);
            quicksort(array, low, i - 1);
            quicksort(array, i + 1, high);
        }
    }

    private int partition(E[] array, int low, int high) {
        E pivot = array[high];
        int partitionNumber = low;

        for (int i = partitionNumber; i < high; i++) {
            if (array[i].compareTo(pivot) < 0) {
                E temp = array[partitionNumber];
                array[partitionNumber] = array[i];
                array[i] = temp;
                partitionNumber++;
            }
        }

        E temp = array[partitionNumber];
        array[partitionNumber] = pivot;
        array[high] = temp;


        return partitionNumber;
    }
}
