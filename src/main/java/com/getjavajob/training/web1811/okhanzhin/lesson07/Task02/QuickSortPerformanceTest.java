package com.getjavajob.training.web1811.okhanzhin.lesson07.Task02;

import java.util.Random;

public class QuickSortPerformanceTest {
    public static void main(String[] args) {
        OneThreadQuickSort<Integer> single = new OneThreadQuickSort<>();
        Integer[] testArray1 = generateArray(10_000_000);

        long startTime = System.currentTimeMillis();

        single.quicksort(testArray1, 0, testArray1.length - 1);
        System.out.println("Single array sorted in " + (System.currentTimeMillis() - startTime) + " ms");

        MultiThreadQuickSort<Integer> multi = new MultiThreadQuickSort<>();
        Integer[] testArray2 = generateArray(10_000_000);

        startTime = System.currentTimeMillis();

        multi.multiThreadQuickSort(testArray2, 0, testArray2.length - 1);
        System.out.println("Threaded array sorted in " + (System.currentTimeMillis() - startTime) + " ms");
    }

    public static Integer[] generateArray(int capacity) {
        Integer[] array = new Integer[capacity];

        int range = 10 * array.length;
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(2 * range) - range;
        }

        return array;
    }
}
