package com.getjavajob.training.web1811.okhanzhin.lesson08.Task01;

public class OneThreadMultiplier {
    public int[][] multiply(int[][] a, int[][] b) {
        int[][] result = new int[a.length][b[0].length];

        for (int row = 0; row < result.length; row++) {
            for (int col = 0; col < result[row].length; col++) {
                result[row][col] = 0;
                for (int i = 0; i < b.length; i++) {
                    result[row][col] += a[row][i] * b[i][col];
                }
            }
        }

        return result;
    }
}
