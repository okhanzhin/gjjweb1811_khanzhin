package com.getjavajob.training.web1811.okhanzhin.lesson09.Task03var01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Processor {
    public static void main(String[] args) {
        Processor pr = new Processor();
        pr.process();
    }

    private void process() {
        AtomicInteger buffer = new AtomicInteger();
        List<Integer> evenNumbers = new ArrayList<>();
        List<Integer> oddNumbers = new ArrayList<>();

        Thread producer = new Thread(new Producer(buffer, 10, new ArrayList<>()));
        producer.start();

        Thread evenConsumer = new Thread(new ConsumerEven(buffer, evenNumbers));
        Thread oddConsumer = new Thread(new ConsumerOdd(buffer, oddNumbers));
        evenConsumer.start();
        oddConsumer.start();

        try {
            producer.join();
            evenConsumer.join();
            oddConsumer.join();
            System.out.println(Arrays.toString(evenNumbers.toArray()));
            System.out.println(Arrays.toString(oddNumbers.toArray()));

        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
