package com.getjavajob.training.web1811.okhanzhin.lesson09.Task01;

import java.io.File;
import java.io.FilenameFilter;

public class ExtensionFileFilter implements FilenameFilter {
    private String extension;

    public ExtensionFileFilter(String extension) {
        this.extension = extension.toLowerCase();
    }

    @Override
    public boolean accept(File file, String name) {
        return name.toLowerCase().endsWith(extension);
    }
}
