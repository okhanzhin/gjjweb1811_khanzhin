package com.getjavajob.training.web1811.okhanzhin.lesson07.Task01;

public class CharSearcher implements Runnable {
    private char letter;
    private String localSource;
    private AlphabetParser alphabetParser;
    private int occurrence;

    public CharSearcher(char letter, String localSource, AlphabetParser alphabetParser) {
        this.letter = letter;
        this.localSource = localSource;
        this.alphabetParser = alphabetParser;
    }

    @Override
    public void run() {
        for (char c : localSource.toCharArray()) {
            if (c == letter) {
                occurrence++;
            }
        }

        alphabetParser.put(letter, occurrence);
    }
}
