package com.getjavajob.training.web1811.okhanzhin.lesson09.Task02;

import java.util.Arrays;

public class Runner {
    public static void main(String[] args) throws Exception {
        StringUtil stringUtil = new StringUtil();
        AlphabetParser alphabetParser = new AlphabetParser(stringUtil.readFile(
                               "/Volumes/Library/dev/projects/getjavajob/web/src/main/resources/text1251.txt"),
                                        stringUtil.readFile(
                               "/Volumes/Library/dev/projects/getjavajob/web/src/main/resources/alphabet1251.txt"));

        MapSorter mapSorter = new MapSorter(alphabetParser.parseByAlphabet());
        System.out.println(Arrays.toString(mapSorter.sortMap().toArray()));
    }
}

