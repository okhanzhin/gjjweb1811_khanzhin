package com.getjavajob.training.web1811.okhanzhin.lesson09.Task03var01;

import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class Producer implements Runnable {
    private static Semaphore prodSem = new Semaphore(1);
    private int iterationNums;
    private AtomicInteger buffer;
    private List<Integer> outputArray;
    private static boolean generationFinished = false;

    public Producer(AtomicInteger buffer, int iterationNums, List<Integer> outputArray) {
        this.buffer = buffer;
        this.iterationNums = iterationNums;
        this.outputArray = outputArray;
    }

    public static void setGenerationFinished(boolean generationFinished) {
        Producer.generationFinished = generationFinished;
    }

    public static boolean isGenerationFinished() {
        return generationFinished;
    }

    public static void acquireSemaphore() {
        try {
            prodSem.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void releaseSemaphore() {
        prodSem.release();
    }

    @Override
    public void run() {
        Random random = new Random();

        for (int i = 0; i < iterationNums; i++) {
            buffer.set(random.nextInt(100));
            outputArray.add(buffer.get());

            if (buffer.get() % 2 == 0) {
                ConsumerEven.releaseSemaphore();
                System.out.println("ConsumerEven Semaphore Released");
            } else {
                ConsumerOdd.releaseSemaphore();
                System.out.println("ConsumerOdd Semaphore Released");
            }
            prodSem.release();

            try {
                Thread.sleep(200);
                prodSem.acquire();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        setGenerationFinished(true);
        ConsumerOdd.releaseSemaphore();
        ConsumerEven.releaseSemaphore();

        System.out.println("Producer is finished");
    }
}