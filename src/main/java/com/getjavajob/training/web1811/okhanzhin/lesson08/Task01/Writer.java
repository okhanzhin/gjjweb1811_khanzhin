package com.getjavajob.training.web1811.okhanzhin.lesson08.Task01;

import java.io.FileWriter;
import java.io.IOException;

public class Writer {
    public void writeToFile(int[][] array, String outputFileName) {
        try {
            FileWriter writer = new FileWriter(outputFileName);

            synchronized (this) {
                for (int i = 0; i < array.length; i++) {
                    for (int j = 0; j < array[0].length; j++) {
                        writer.write(array[i][j] + " ");
                    }
                    writer.write("\n");
                }
                writer.write("\n");
                writer.write("\n");
            }

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
