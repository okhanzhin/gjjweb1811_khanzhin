package com.getjavajob.training.web1811.okhanzhin.lesson07.Task02;

public class MultiThreadQuickSort<E extends Comparable<E>> {
    private final static int NUM_THREADS = Runtime.getRuntime().availableProcessors();
    private static int count = 0;

    public void multiThreadQuickSort(E[] array, int low, int high) {
        if (array.length == 0) {
            throw new IllegalArgumentException("Error at MultiThreadQuickSort::quicksort. Input array of data to be sorted is empty or null.");
        } else if (high > low) {
            int i = this.partition(array, low, high);
            if (count < NUM_THREADS) {
                count++;
                Thread quicksortLeft = new Thread(new SortThread<>(array, low, i - 1));
                quicksortLeft.start();
                Thread quicksortRight = new Thread(new SortThread<>(array, i + 1, high));
                quicksortRight.start();
                try {
                    quicksortLeft.join();
                    quicksortRight.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private int partition(E[] array, int low, int high) {
        E pivot = array[high];

        for (int i = low; i < high; i++) {
            if (array[i].compareTo(pivot) < 0) {
                E temp = array[low];
                array[low] = array[i];
                array[i] = temp;
                low++;
            }
        }

        E temp = array[low];
        array[low] = pivot;
        array[high] = temp;

        return low;
    }

    public class SortThread<V extends Comparable<E>> implements Runnable {
        private V[] array;
        private int low;
        private int high;

        public SortThread(V[] array, int low, int high) {
            this.array = array;
            this.low = low;
            this.high = high;
        }

        @Override
        public void run() {
            multiThreadQuickSort((E[]) array, low, high);
        }
    }
}
