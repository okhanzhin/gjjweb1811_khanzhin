package com.getjavajob.training.web1811.okhanzhin.lesson09.Task03var01;

import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class ConsumerEven implements Runnable {
    private static Semaphore evenSem = new Semaphore(0);
    private AtomicInteger buffer;
    List<Integer> outputArray;

    public ConsumerEven(AtomicInteger buffer, List<Integer> outputArray) {
        this.buffer = buffer;
        this.outputArray = outputArray;
    }

    public static void releaseSemaphore() {
        evenSem.release();
    }

    @Override
    public void run() {
        while (true) {
            try {
                evenSem.acquire();

                if (Producer.isGenerationFinished()) {
                    break;
                }

                Producer.acquireSemaphore();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            outputArray.add(buffer.get());
            System.out.println("Even " + outputArray.get(outputArray.size() - 1));

            Producer.releaseSemaphore();
            System.out.println("Producer Sem released");

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("ConsumerEven is finished");
    }
}
