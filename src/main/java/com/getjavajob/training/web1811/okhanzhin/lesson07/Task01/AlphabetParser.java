package com.getjavajob.training.web1811.okhanzhin.lesson07.Task01;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AlphabetParser {
    private final String source;
    private final String alphabet;
    private Map<Character, Integer> charMap = new HashMap<>();

    public AlphabetParser(String source, String alphabet) {
        this.source = source;
        this.alphabet = alphabet;
    }

    public Map<Character, Integer> getCharMap() {
        return charMap;
    }

    public Map<Character, Integer> parseByAlphabet() throws InterruptedException {
        List<Thread> threadList = new ArrayList<>();

        for (char c : alphabet.toCharArray()) {
            CharSearcher charSearcher = new CharSearcher(c, source, this);
            Thread searchThread = new Thread(charSearcher);
            threadList.add(searchThread);
            searchThread.start();
        }

        for (Thread thread : threadList) {
            thread.join();
        }

        return charMap;
    }

    protected synchronized void put(char charToPut, int occurrence) {
        charMap.merge(charToPut, occurrence, Integer::sum);
    }
}
