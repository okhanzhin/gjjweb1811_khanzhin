package com.getjavajob.training.web1811.okhanzhin.lesson09.Task02;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class AlphabetParser {
    private final String source;
    private final String alphabet;

    public AlphabetParser(String source, String alphabet) {
        this.source = source;
        this.alphabet = alphabet;
    }

    public Map<Character, Integer> parseByAlphabet() throws InterruptedException {
        Map<Character, Future<Integer>> futureMap = new ConcurrentHashMap<>();
        Map<Character, Integer> charMap = new ConcurrentHashMap<>();
        char[] chars = alphabet.toCharArray();

        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
        CountDownLatch cd = new CountDownLatch(chars.length);

        runAllThreads(futureMap, chars, cachedThreadPool, cd);

        cd.await();
        System.out.println("All threads are finished");

        convertMap(futureMap, charMap);

        cachedThreadPool.shutdown();

        return charMap;
    }

    private void runAllThreads(Map<Character, Future<Integer>> futureMap, char[] chars, ExecutorService cachedThreadPool, CountDownLatch cd) {
        for (char c : chars) {
            Future<Integer> future = cachedThreadPool.submit(new CountDownCharSearcher(c, source, cd));
            futureMap.put(c, future);
        }
    }

    private void convertMap(Map<Character, Future<Integer>> futureMap, Map<Character, Integer> charMap) throws InterruptedException {
        try {
            for (Map.Entry<Character, Future<Integer>> entry : futureMap.entrySet()) {
                Character entryKey = entry.getKey();
                Future<Integer> entryValue = entry.getValue();
                charMap.put(entryKey, entryValue.get());
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
