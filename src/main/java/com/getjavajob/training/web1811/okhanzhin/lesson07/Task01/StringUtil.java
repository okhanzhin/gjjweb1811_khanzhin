package com.getjavajob.training.web1811.okhanzhin.lesson07.Task01;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class StringUtil {
    public String prepareString(String fileName) throws IOException {
        String resultString;

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "windows-1251"))) {
            StringBuilder stringBuilder = new StringBuilder();

            while ((resultString = bufferedReader.readLine()) != null) {
                stringBuilder.append(resultString);
            }

            resultString = stringBuilder.toString();
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("Error at StringConverter::prepareString. Input file is invalid. " + e.getMessage());
        }
        return resultString;
    }
}
