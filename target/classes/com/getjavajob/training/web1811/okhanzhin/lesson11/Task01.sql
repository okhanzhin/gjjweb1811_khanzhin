SELECT ROW_NUMBER() OVER (ORDER BY ID) num, ID, NAME 
FROM Products_tbl; 

SELECT @i := @i + 1 num, ID, NAME 
FROM products_tbl, (SELECT @i := 0) X

SELECT COUNT(*) num, P2.ID, P2.NAME 
FROM products_tbl P1 JOIN 
products_tbl P2 ON P1.ID <= P2.ID 
GROUP BY P2.ID;