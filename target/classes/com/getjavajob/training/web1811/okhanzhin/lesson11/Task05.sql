SELECT t1.id + 1 AS missing_id FROM 
names AS t1 
LEFT JOIN names AS t2 
ON t2.id = t1.id +1 
WHERE t2.id IS NULL 
LIMIT 1;